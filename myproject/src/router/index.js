import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home
  },
  {
    path: '/form',
    name: 'form',
    component: () => import('../views/Form.vue')
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ '../views/About.vue')
  },

  {
    path: '/showview',
    name: 'Showview',
    component: () => import('../views/Showview.vue')
  },

  {
    path: '/table',
    name: 'Table',
    component: () => import('../views/Table.vue')
  },

  {
    path: '/counter',
    name: 'counter',
    component: () => import('../views/Counter.vue')
  },

  {
    path: '/users',
    name: 'users',
    component: () => import('../views/Users')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
